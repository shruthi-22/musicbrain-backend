import json
import os
from datetime import date, datetime

import requests
from dotenv import load_dotenv

load_dotenv()


class MusicBrain:

    @staticmethod
    def get_access_token(refresh_token):
        url = "https://accounts.spotify.com/api/token"

        payload = os.environ.get('key') + refresh_token
        headers = {
            'content-type': "application/x-www-form-urlencoded"
        }

        response = requests.request("POST", url, data=payload, headers=headers)
        access_token = json.loads(response.content)['access_token']
        return access_token

    def __init__(self, refresh_token):
        self.token = MusicBrain.get_access_token(refresh_token)

        self.headers = {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + str(self.token)
        }

    def get_liked_songs(self):
        url = "https://api.spotify.com/v1/me/tracks?market=IN&limit=50"
        output = self.make_api_call(url)
        liked_songs = output['items']
        while output['next'] is not None:
            output = self.make_api_call(output['next'])
            liked_songs += output['items']
        return liked_songs

    def get_recently_played(self):
        url = "https://api.spotify.com/v1/me/player/recently-played?type=track&limit=50"
        output = self.make_api_call(url)
        recent_songs = output['items']
        while output['next'] is not None:
            output = self.make_api_call(output['next'])
            recent_songs += output['items']
        return recent_songs

    def make_api_call(self, url):
        result = requests.request("GET", url, headers=self.headers)
        output = json.loads(result.content)
        return output

    @staticmethod
    def extract_song_details(song_list, status):
        data = set()
        for i in song_list:
            song_id = i['track']['id']
            if status:
                data.add((i['track']['name'].replace('"', "'"),
                          i['track']['id'], status, i['added_at']))
            else:
                data.add((i['track']['name'].replace('"', "'"),
                          i['track']['id'], status, i['played_at']))
        return data

    def get_song_features(self, song_id, song_name):

        url = "https://api.spotify.com/v1/audio-features/" + song_id
        analysis = self.make_api_call(url)
        return (song_id, song_name, analysis['danceability'], analysis['energy'],
                analysis['key'], analysis['loudness'], analysis['mode'],
                analysis['speechiness'], analysis['instrumentalness'],
                analysis['valence'], analysis['acousticness'], analysis['tempo'])

    def prepare_song_list(self):
        liked = self.get_liked_songs()
        song_list = MusicBrain.extract_song_details(liked, 1)
        recent = self.get_recently_played()
        for i in MusicBrain.extract_song_details(recent, 0):
            song_list.add((i[0], i[1], i[2], i[3]))
        response = []
        count = 0
        for i in sorted(song_list):
            try:
                response.append({"song_name": i[0], "song_id": i[1], "status": i[2], "timestamp": datetime.strptime(
                    i[3], '%Y-%m-%dT%H:%M:%S.%fZ')})
            except ValueError:
                response.append({"song_name": i[0], "song_id": i[1], "status": i[2], "timestamp": datetime.strptime(
                    i[3], '%Y-%m-%dT%H:%M:%SZ')})
            count += 1

        return {"status": 1, "count": count, "songs": response}

    # playlist are fetched based on genre, language and other features
    def fetch_playlists(self, num, seed, data):
        danceability = round(data['danceability'].mean(), 2)
        key = int(data['s_key'].mean())
        energy = round(data['energy'].mean(), 2)
        loudness = round(data['loudness'].mean(), 2)
        speechiness = round(data['speechiness'].mean(), 2)
        acousticness = round(data['acousticness'].mean(), 2)
        instrumentalness = round(data['instrumentalness'].mean(), 2)
        valance = round(data['valance'].mean(), 2)
        tempo = round(data['tempo'].mean())
        url = "https://api.spotify.com/v1/recommendations?limit=" + str(num) + "&seed_tracks=" + str(
            seed) + "&target_danceability=" + str(danceability) + "&target_energy=" + str(
            energy) + "&target_instrumentalness=" + str(
            instrumentalness) + "&target_key=" + str(key) + "&target_loudness=" + str(
            loudness) + "&target_speechiness=" + str(speechiness) + "&target_tempo=" + str(
            tempo) + "&target_valence=" + str(valance) + "&target_acousticness=" + str(acousticness)
        output = self.make_api_call(url)
        for i in range(num):
            yield self.get_song_features(output["tracks"][i]["id"], output["tracks"][i]["name"])

    def get_id(self):
        url = "https://api.spotify.com/v1/me"
        output = self.make_api_call(url)
        return output['id']

    def create_playlist(self):
        url = "https://api.spotify.com/v1/users/" + str(self.get_id()) + "/playlists"
        payload = '{\r\n  \"name\": \"MusicBrain ' + str(date.today()) + \
                  '\",\r\n  \"description\": \"A freshly brewed suggestion from MusicBrain\",\r\n  \"public\": false\r\n}'
        response = requests.request("POST", url, data=payload, headers=self.headers)
        output = json.loads(response.content)
        return output['id']

    def add_songs_to_playlist(self, playlist_id, song_ids):
        url = "https://api.spotify.com/v1/playlists/" + playlist_id + "/tracks"

        tracks = ["spotify:track:" + str(x) for x in song_ids]
        payload = {"uris": tracks}
        response = requests.request("POST", url, data=json.dumps(payload), headers=self.headers)
        output = json.loads(response.content)
        return output is not None
