import mysql.connector as sql
from flask import Blueprint, request

from .database import access_database

db = access_database()

posts = Blueprint('posts', __name__, url_prefix="/posts")

# Buffered when set to true allows multiple queries without collision
cursor = db.cursor(buffered=True)


@posts.route('/new', methods=['POST'])
def new():
    data = request.get_json()
    try:
        username = data['username']
        song_id = data['song_id']
        caption = data['caption']
        try:
            cursor.execute(
                "insert into posts(username,song_id,caption) values(%s,%s,%s)", (username, song_id, caption))
            db.commit()
            return ({"status": 1, "message": " Your post is up !! "}), 200
        except sql.errors.IntegrityError:
            return ({"status": 0, "message": "Invalid username or song id"}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@posts.route('/delete', methods=['DELETE'])
def delete():
    data = request.get_json()
    try:
        username = data['username']
        post_id = data['post_id']
        cursor.execute("delete from comments where post_id=%s", (post_id,))
        cursor.execute(
            "delete from posts where username=%s and post_id=%s", (username, post_id))
        if cursor.rowcount == 1:
            db.commit()
            return ({"status": 1, "message": "Post deleted"}), 200
        else:
            return ({"status": 0, "message": "Something went wrong"}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@posts.route('/edit', methods=['POST'])
def edit():
    data = request.get_json()
    try:
        username = data['username']
        post_id = data['post_id']
        song_id = data['song_id']
        caption = data['caption']
        try:
            cursor.execute("update posts set caption=%s, song_id=%s where username=%s and post_id=%s",
                           (caption, song_id, username, post_id))
            if cursor.rowcount == 1:
                db.commit()
                return ({"status": 1, "message": "Post edited !"}), 200
            else:
                return ({"status": 0, "message": "Something went wrong"}), 200
        except sql.errors.IntegrityError:
            return ({"status": 0, "message": "Song does not exist"}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@posts.route('/like', methods=['PUT'])
def like():
    post_id = request.args.get('post_id')
    if post_id is not None:
        cursor.execute(
            "update posts set like_count=like_count+1 where post_id=%s", (post_id,))
        if cursor.rowcount == 1:
            db.commit()
            return ({"status": 1, "message": " liked"}), 200
        else:
            return ({"status": 0, "message": "Something went wrong"}), 400
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


def generate_posts(posts_list):
    for i in cursor:
        post = {'post_id': i[0], 'username': i[1], 'song_id': i[2], 'caption': i[3], 'like_count': i[4],
                'timestamp': i[5]}
        posts_list.append(post)


@posts.route('/profile', methods=['GET'])
def profile():
    username = request.args.get('username')
    if username is not None:
        cursor.execute("select * from  users where username=%s", (username,))
        if cursor.rowcount == 1:
            posts_list = []
            cursor.execute(
                "select * from posts where username=%s order by post_id desc", (username,))
            generate_posts(posts_list)
            return ({"status": 1, "posts": posts_list}), 200
        else:
            return ({"status": 0, "message": "User doesn't exist"}), 400
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@posts.route('/feed', methods=['GET'])
def feed():
    username = request.args.get('username')
    if username is not None:
        cursor.execute("select * from  users where username=%s", (username,))
        if cursor.rowcount == 1:
            posts_list = []
            # display my friends posts
            cursor.execute(
                (
                    "select * from posts where username in "
                    "(select friend from friends where username=%s and status=%s) order by post_id desc"),
                (username, 1))
            generate_posts(posts_list)
            return ({"status": 1, "posts": posts_list}), 200
        else:
            return ({"status": 0, "message": "User doesn't exist"}), 400
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400
