from datetime import date
from sys import path

from flask import Blueprint, request

path.insert(0, 'core')
path.insert(0, 'routes')
import core.collab_reco as collab
from core.analysis import MusicBrain
from routes.database import access_database

db = access_database()

trends = Blueprint('trends', __name__, url_prefix="/trends")

# Buffered when set to true allows multiple queries without collision
cursor = db.cursor(buffered=True)


@trends.route('/mood', methods=['POST'])
def show_trends():
    data = request.get_json()
    if data is not None:
        try:
            return ({"status": 1, "songs": collab.chart(data['username'], [7, 2, 10, 30])}), 200
        except KeyError:
            return ({"status": 0, "message": "Invalid parameters passed"}), 400
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@trends.route('/top10', methods=['GET'])
def top10():
    username = request.args.get('username')
    if username is not None:
        top10_Global_songName = []
        top10_Global_songCount = []
        top10_myFriends_songName = []
        top10_myFriends_songCount = []
        try:
            # top 10 global
            cursor.execute(
                "select T.cnt,songs.name from (SELECT COUNT(song_id) as cnt,song_id FROM activity "
                "where status=0 GROUP BY song_id) as T JOIN songs ON T.song_id=songs.song_id ORDER by T.cnt desc limit 10")
            for song in cursor:
                top10_Global_songName.append(song[1])
                top10_Global_songCount.append(song[0])
            # top 10 among my friends
            cursor.execute(
                "select T.cnt,songs.name from (SELECT COUNT(song_id) as cnt,song_id FROM activity"
                " where status=0 and username in (select friend from friends where username=%s)"
                "GROUP BY song_id) as T JOIN songs ON T.song_id=songs.song_id ORDER by T.cnt desc limit 10",
                (username,))
            for song in cursor:
                top10_myFriends_songName.append(song[1])
                top10_myFriends_songCount.append(song[0])
            return ({"status": 1, "top10_Global_songName": top10_Global_songName,
                     "top10_Global_songCount": top10_Global_songCount,
                     "top10_myFriends_songName": top10_myFriends_songName,
                     "top10_myFriends_songCount": top10_myFriends_songCount}), 200
        except:
            return ({"status": 0, "error": "Sql error"}), 400
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@trends.route('/weeklyAct', methods=['GET'])
def past7daysActivity():
    username = request.args.get('username')
    if username is not None:
        countFor7Days = [0] * 7
        index = date.today().day - 6
        try:
            cursor.execute(
                "select date(timestamp),count(*) as dt from activity "
                "where username=%s and status=0 and curdate()-date(timestamp) <=7 group by date(timestamp)",
                (username,))
            for countData in cursor:
                countFor7Days[countData[0].day - index] = countData[1]
            countFor7Days.reverse()
            return ({"status": 1, "countFor7Days": countFor7Days}), 200
        except:
            return ({"status": 0, "error": "Sql error"}), 400
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@trends.route('/colab_reco', methods=['POST'])
def collab_reco():
    data = request.get_json()
    try:
        username = data['username']
        token = data['token']
        reco = collab.recommend(username)
        mb = MusicBrain(token)
        playlist_id = mb.create_playlist()
        mb.add_songs_to_playlist(playlist_id, reco)
        cursor.execute("update users set collab_id=%s where username=%s;", (playlist_id, username))
        for song in reco:
            cursor.execute("insert into recommendations(playlist_id,song_id) values(%s,%s)", (playlist_id, song))
        db.commit()
        return ({"status": 1, "playlist": playlist_id}), 200
    except TypeError:
        return ({"status": 0, "message": "Sql Error"}), 400
