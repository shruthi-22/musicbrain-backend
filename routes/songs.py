from sys import path

import mysql.connector as sql
from flask import Blueprint, request

path.insert(0, 'core')
path.insert(0, 'routes')
from routes.database import access_database
from core.analysis import MusicBrain
from core.songs import recommendation_engine

db = access_database()

songs = Blueprint('songs', __name__, url_prefix="/songs")

# Buffered when set to true allows multiple queries without collision
cursor = db.cursor(buffered=True)


@songs.route('/', methods=['GET'])
def user_song_list():
    username = request.args.get('username')
    if username is not None:
        song_list = []
        cursor.execute(
            (
                "select distinct(a.song_id),name from activity a,songs where a.song_id=songs.song_id and username=%s "
                "order by name;"),
            (username,))
        for song in cursor:
            song_list.append({"song_id": song[0], "song_name": song[1]})

        return ({"status": 1, "count": cursor.rowcount, "songs": song_list}), 200
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@songs.route('/activity', methods=['POST'])
def user_activity():
    data = request.get_json()
    try:
        refresh_token = data['token']
        username = data['username']
        mb = MusicBrain(refresh_token)
        count = 0
        for song in mb.prepare_song_list()['songs']:
            try:
                cursor.execute("replace into activity values(%s,%s,%s,%s)",
                               (username, song['song_id'], song['status'], song['timestamp']))
                count += 1
            except sql.errors.IntegrityError:
                cursor.execute("insert into songs values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                               mb.get_song_features(song['song_id'], song['song_name']))
                cursor.execute("replace into activity values(%s,%s,%s,%s)",
                               (username, song['song_id'], song['status'], song['timestamp']))
                count += 1
            except sql.errors.DataError:
                continue
            except:
                continue

        db.commit()
        return ({"status": 1, "count": count}), 200

    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@songs.route('/recommend', methods=['POST'])
def recommendations():
    data = request.get_json()
    try:
        refresh_token = data['token']
        username = data['username']
        status, playlist_id = recommendation_engine(username, refresh_token)
        cursor.execute("update users set playlist_id=%s where username=%s;", (playlist_id, username))
        db.commit()
        return ({"status": int(status), "message": playlist_id}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@songs.route('/reco-playlist', methods=['GET'])
def get_reco_playlist():
    username = request.args.get('username')
    if username is not None:
        cursor.execute("select playlist_id,collab_id from users where username=%s;", (username,))
        playlist_id, collab_id = cursor.fetchone()
        if playlist_id is not None and collab_id is not None:
            return ({"status": 1, "playlist_id": playlist_id, "collab_id": collab_id}), 200
        elif playlist_id is not None:
            return ({"status": 1, "playlist_id": playlist_id}), 200
        elif collab_id is not None:
            return ({"status": 1, "collab_id": collab_id}), 200
        else:
            return ({"status": 0, "message": "No recommendation generated for current user"}), 200
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400
