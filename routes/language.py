import mysql.connector as sql
from flask import Blueprint, request

from .database import access_database

db = access_database()

language = Blueprint('language', __name__, url_prefix="/language")

# Buffered when set to true allows multiple queries without collision
cursor = db.cursor(buffered=True)


@language.route('/add', methods=['POST'])
def add_language():
    data = request.get_json()
    try:
        language = data['language']
        username = data['username']
        if isinstance(language, list):
            for item in language:
                cursor.execute(
                    "insert into languages(username,language) values(%s,%s)", (username, item))
            db.commit()
            return ({"status": 1, "message": "Language added"}), 200
        else:
            return ({"status": 0, "message": "Invalid format,got string expected list"}), 400
    except sql.errors.IntegrityError:
        return ({"status": 0, "message": "Duplicate entry"}), 200
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@language.route('/delete', methods=['DELETE'])
def delete_language():
    data = request.get_json()
    try:
        username = data['username']
        language = data['language']
        if isinstance(language, list):
            for item in language:
                cursor.execute(
                    "delete from languages where username=%s and language=%s", (username, item))
            db.commit()
            if cursor.rowcount >= 1:
                return ({"status": 1, "message": "Language preference deleted"}), 200
            else:
                return ({"status": 0, "message": "Username or language doesn't exist"}), 400
        else:
            return ({"status": 0, "message": "Invalid format,got string expected list"}), 400
    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@language.route('/view', methods=['GET'])
def view_languages():
    username = request.args.get('username')
    if username is not None:
        languages = []
        cursor.execute(
            "select language from languages where username=%s;", (username,))
        for language in cursor:
            languages.append(language[0])
        return ({"status": 1, "languages": languages}), 200
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400
