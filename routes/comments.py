import mysql.connector as sql
from flask import Blueprint, request

from .database import access_database

comments = Blueprint('comments', __name__, url_prefix="/comments")
db = access_database()
# Buffered when set to true allows multiple queries without collision
cursor = db.cursor(buffered=True)


@comments.route('/all', methods=['GET'])
def all_comments():
    post_id = request.args.get("post_id")
    if post_id is not None:
        comments_list = []
        query = "select *from comments wh"
        cursor.execute("select *from comments where post_id=%s", (post_id,))
        for comment in cursor:
            comments_list.append(
                {"comment_id": comment[0], "message": comment[2], "username": comment[3], "timestamp": comment[4]})
        return {"status": 1, "comments": comments_list}
    else:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400


@comments.route('/add', methods=['POST'])
def add_new_comment():
    data = request.get_json()
    try:
        post_id = data['post_id']
        username = data['username']
        message = data['message']
        try:
            cursor.execute(
                "insert into comments(post_id,username,message) values(%s,%s,%s)", (post_id, username, message))
            db.commit()
            return ({"status": 1, "message": "Comment created"}), 200
        except sql.errors.IntegrityError:
            return ({"status": 0, "message": "Comment could not be created. Invalid post_id."}), 200

    except KeyError:
        return ({"status": 0, "message": "Invalid parameters passed"}), 400
