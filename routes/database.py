# Database connector
import os

import mysql.connector as sql
from dotenv import load_dotenv

db = None


def establish_connection():
    global db
    # Load env variables
    load_dotenv()
    user = os.environ.get("db.user")
    database = os.environ.get("db.name")
    host = os.environ.get("db.host")
    passwd = os.environ.get("db.passwd")
    db = sql.connect(user=user, database=database, host=host, passwd=passwd)


def access_database():
    global db
    return db


def check_connection():
    global db
    if db.is_connected():
        return 1
    else:
        db.reconnect(3, 0.1)
        return 0
